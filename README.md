# [cakephp/chronos](https://phppackages.org/p/cakephp/chronos)

A simple immutable API extension for DateTime. https://book.cakephp.org/3.0/en/chronos.html

## See also
* https://phppackages.org/p/warhuhn/chronos-doctrine